    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Guru</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                  <div class="row mb-2">
                    <div class="col-sm">
                       <h3>Guru</h3>
                    </div>
                    <div class="col-sm">
                      <div class="float-sm-right">
                        <a class="btn btn-success" href="/layout-tambah-guru" role="button">Tambah Data</a>
                      </div>
                    </div>
                  </div>
              </div>

              <!-- /.card-header -->
              <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>NIP</th>
                    <th>Alamat</th>
                    <th>Email</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>1</td>
                    <td>Eko Sulistyono
                    </td>
                    <td>1234567890</td>
                    <td>ekosulistyono@email.com</td>
                    <td>Tegalrejo</td>
                    <td>
                      <button type="button" class="btn btn-info btn-sm">Edit</button>
                      <button type="button" class="btn btn-danger btn-sm">Delete</button>
                    </td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Yusuf Fauzan</td>
                    <td>097651238</td>
                    <td>fauzan.yusuf@email.com</td>
                    <td>Blabak</td>
                    <td>
                      <button type="button" class="btn btn-info btn-sm">Edit</button>
                      <button type="button" class="btn btn-danger btn-sm">Delete</button>
                    </td>
                  </tr>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
