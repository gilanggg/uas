    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Siswa</h1>
          </div>
          <div class="col-sm-6">
            <div class="float-sm-right">
              <a class="btn btn-success" href="/layout-tambah-siswa" role="button">Tambah Data</a>
            </div>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>


    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data siswa</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>NIS</th>
                    <th>Alamat</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>1</td>
                    <td>Gilang Arinanto</td>
                    <td>2105040030</td>
                    <td>Perum Depkes, Kota Magelang</td>
                    <td>
                      <button type="button" class="btn btn-info btn-sm">Edit</button>
                      <button type="button" class="btn btn-danger btn-sm">Delete</button>
                    </td>
                  </tr>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
