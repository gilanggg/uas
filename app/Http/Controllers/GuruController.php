<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GuruController extends Controller
{
    //
    public function guru(){
        return view('/guru.layout');
    }

    public function tambah_guru(){
        return view('guru.layout-tambah-guru');
    }
    
}