<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    //
    public function index(){
        return view('/admin');
    }

    public function create(){
        return view('user.layout-create');
    }

    public function login(){
        return view('user.layout-user');
    }
}
