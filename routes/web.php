<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layout');
});

use App\Http\Controllers\AdminController;
Route::get('/admin', [AdminController::class, 'index']);
Route::get('/create-admin', [AdminController::class, 'create']);
Route::get('/login', [AdminController::class, 'login']);

use App\Http\Controllers\GuruController;
Route::get('/guru', [GuruController::class, 'guru']);
Route::get('/layout-tambah-guru', [GuruController::class, 'tambah_guru']);

use App\Http\Controllers\SiswaController;
Route::get('/siswa', [SiswaController::class, 'siswa']);
Route::get('/layout-tambah-siswa', [SiswaController::class, 'tambah_siswa']);
